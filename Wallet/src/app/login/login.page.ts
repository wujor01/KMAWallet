import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { ApiService } from "../api.service";
import { HubService } from '../hub.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  myInfo = localStorage.myInfo ? JSON.parse(localStorage.myInfo) : {};

  constructor(
    private apiService: ApiService, 
    private router: Router, 
    private hub: HubService, 
    private faio: FingerprintAIO,
    private plat: Platform) { }
  ngOnInit(): void {
    if(localStorage.token && !this.plat.is('desktop')){
      this.showFingeerprintAuthentication();
    }
  }

  fnLogin(form) {
    const body = form.value;
    console.log(body);
    this.apiService.signIn(body)
    .then(res => {
      if(localStorage.token != res.token){
        this.hub.sendAuth(res.token);
      }
            
      localStorage.token = res.token;
      console.log(res);
      
      this.router.navigate(['/tabs']);
    })
    .catch(err => () => {alert(err.error); localStorage.clear();} );
  }

  public showFingeerprintAuthentication() {
    if (localStorage.token == undefined) {
      alert("Bạn chưa từng đăng nhập trước đây, vui lòng đăng nhập!");
      return;
    }

    this.faio.isAvailable().then((result: any) => {
      console.log(result)

      this.faio.show({
        cancelButtonTitle: 'Hủy',
        //description: "Some biometric description",
        disableBackup: true,
        title: 'Xác thực ví',
        fallbackButtonTitle: 'FB Back Button',
        subtitle: 'Dùng finger để tiếp tục'
      })
        .then((result: any) => {
          console.log(result);
          this.router.navigate(['/tabs']);
        })
        .catch((error: any) => {
          console.log(error);
        });

    })
      .catch((error: any) => {
        console.log(error)
      });
  }
}
