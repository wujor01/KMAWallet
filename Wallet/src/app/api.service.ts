import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { UserModel } from './model/user/user-model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) {}
  
  private readonly url :string = environment.urlEndpoint;
  
  /**Khởi tạo header */
  newHeaders(){
    if(localStorage.token){
      return new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Origin', '*')
      .set('Authorization', 'Bearer ' + localStorage.token);
    }
      return new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Origin', '*');
  }
  
  /** POST đăng nhập */
  signIn(bodyData){
    const url = this.url + '/api/Users/Login';

    return this.http.post<UserModel>(url, bodyData, {'headers' : this.newHeaders()})
    .toPromise();
  }

  /** GET All users */
  getAllUsers(){
    const url = this.url + '/api/Users/GetAll';
    return this.http.get(url, {'headers' : this.newHeaders()})
    .toPromise();
  }

  getMyInfo(){
    const url = this.url + '/api/Users/GetMyInfo';
    return this.http.get(url, {'headers' : this.newHeaders()})
    .toPromise();
  }

  getMyWallets(){
    const url = this.url + '/api/Wallet/GetMyWallets';
    return this.http.get(url, {'headers' : this.newHeaders()})
    .toPromise();
  }

  getAllMyTransaction(){
    const url = this.url + '/api/Wallet/GetAllMyTransaction';
    return this.http.get(url, {'headers' : this.newHeaders()})
    .toPromise();
  }

  postNewTransaction(param){
    let httpParams = new HttpParams()
    .append("walletid", param.walletid)
    .append("toWalletid", param.toWalletid)
    .append("amount", param.amount)
    .append("content", param.content);

    const url = this.url + '/api/Wallet/NewTransaction';
    return this.http.post(url, null, {'headers' : this.newHeaders(), 'params' : httpParams})
    .toPromise();
  }

  getUserByWalletId(walletid){
    let httpParams = new HttpParams()
    .append("walletid", walletid);
    const url = this.url + '/api/Users/GetUserByWalletId';
    return this.http.get(url, {'headers' : this.newHeaders(), 'params' : httpParams})
    .toPromise();
  }

  postVerifyTransaction(result){
    const url = this.url + '/api/Wallet/VerifyTransaction';
    return this.http.post(url, result, {'headers' : this.newHeaders(), 'responseType' : 'text'})
    .toPromise();
  }
}
