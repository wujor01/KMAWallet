import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgxQrcodeErrorCorrectionLevels,NgxQrcodeElementTypes } from '@techiediaries/ngx-qrcode';
import { HubService } from './../hub.service';
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(private router: Router, private hub:HubService) {}

  
  fnSendMessage(form) {
    const body = form.value;
    console.log(body);
    this.hub.sendMessage(body.username, body.message);
  }
}
