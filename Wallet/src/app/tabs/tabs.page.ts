import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ApiService } from './../api.service';
import { HubService } from './../hub.service';
import { LoadingService } from './../loading.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {

  constructor(private router: Router, private api: ApiService, private hub: HubService, public loading : LoadingService) {}  

  ngOnInit(): void {
    this.fnLoad();
  }

  async fnLoad(){
    if (localStorage.token == undefined || localStorage.token == null) {
      this.router.navigate(['/login']);
    }else{
      this.loading.present();
      //console.log(localStorage.token);
      this.api.getMyInfo()
      .then(res =>{
        localStorage.myInfo = JSON.stringify(res);
        console.log(res);

        this.api.getMyWallets()
        .then(res =>{
          this.loading.dismiss();
          localStorage.myWallets = JSON.stringify(res);
          console.log(res);
        })
        .catch(err => {
          alert(JSON.stringify(err));
          this.loading.dismiss();
        });
      })
      .catch(err => {
        alert(err.error);
        this.loading.dismiss();
      });

      
    }
  }
}
