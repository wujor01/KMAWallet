import { Component } from '@angular/core';
import { HubService } from './../hub.service';
import { ApiService } from './../api.service';
import { NavigationEnd, Router } from '@angular/router';
import { LoadingService } from '../loading.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page{
  
  listTransaction;

  constructor(private hub: HubService, private api: ApiService, private router: Router, public loading: LoadingService) {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        if(ev.urlAfterRedirects == '/tabs/tab2'){
          this.InitData();
        }
    }
  });
}
  InitData(): void {
    debugger;
    this.loading.present();
    this.api.getAllMyTransaction()
      .then(res =>{
        this.listTransaction = res;
        this.loading.dismiss();
      })
      .catch(err => {
        alert(err);
        this.loading.dismiss();
      });
  }
}

