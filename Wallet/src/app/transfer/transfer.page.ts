import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { LocalNotifications } from '@capacitor/local-notifications';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { ModalController, Platform } from '@ionic/angular';
import { LoadingService } from '../loading.service';
import { ApiService } from './../api.service';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.page.html',
  styleUrls: ['./transfer.page.scss'],
})
export class TransferPage implements OnInit {

  @Input() qrResult;
  towalletid: string;
  touser: string;
  myWallets = JSON.parse(localStorage.myWallets);
  isInputForm:boolean = true;
  result;

  constructor(private api:ApiService, public modalController: ModalController, private platform: Platform, private faio: FingerprintAIO, private plat: Platform, public loading: LoadingService, private router: Router) { }

  ngOnInit() {
    this.reloadWallets();
    this.towalletid = this.qrResult ? this.qrResult.walletid : undefined;
    this.touser = this.qrResult ? this.qrResult.fullname : undefined;
  }

  fnNewTransaction(form) {
    this.loading.present();
    const body = form.value;
    body.walletid = body.walletid;
    this.api.postNewTransaction(body)
    .then(res =>{
      this.loading.dismiss();
      this.result = res;
      this.isInputForm = false;
      console.log(res);
    })
    .catch(err => {
      alert(err.error); 
      this.loading.dismiss();
    });
  }

  reloadWallets(){
    this.loading.present();
    this.api.getMyWallets()
    .then(res =>{
      this.loading.dismiss();
      this.myWallets = res;
      localStorage.myWallets = JSON.stringify(res);
    })
    .catch(err => {
      alert(JSON.stringify(err)); 
      this.loading.dismiss();
    });
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  sendNotify(res){
    debugger;
    if(this.platform.is("mobile")){
      LocalNotifications.schedule({
        notifications: [
          {
            title: 'Thông báo',
            body: res,
            id: 2,
            extra: {
              data: res
            },
            iconColor: '#0000FF'
          }
        ]
      })
    }else{
      alert(JSON.stringify(res));
    }
    
    this.redirectToHome();
  }

  onSubmit(flag){
    if(flag){
      if(!this.plat.is('desktop')){
        this.showFingeerprintAuthentication(this.result);
      }else{
        this.verifyTransaction(this.result);
      }
    }else{
      this.isInputForm = true;
      console.log('Hủy');
    }
  }

  showFingeerprintAuthentication(postBody) {
    this.faio.isAvailable().then((result: any) => {
      console.log(result)

      this.faio.show({
        cancelButtonTitle: 'Hủy',
        //description: "Some biometric description",
        disableBackup: true,
        title: 'Xác thực ví',
        fallbackButtonTitle: 'FB Back Button',
        subtitle: 'Dùng finger để tiếp tục'
      })
        .then((result: any) => {
          console.log(result);
          this.verifyTransaction(postBody);
        })
        .catch((error: any) => {
          console.log(error);
        });

    })
      .catch((error: any) => {
        console.log(error)
      });
  }

  verifyTransaction(result){
    debugger;
    this.loading.present();
    this.api.postVerifyTransaction(result)
          .then(res =>{
            this.loading.dismiss();
            this.sendNotify(res);
          })
          .catch(err => {
            alert(JSON.stringify(err));
            this.loading.dismiss();
          });
  }

  redirectToHome(){
    this.router.navigateByUrl('tabs');
  }
}
