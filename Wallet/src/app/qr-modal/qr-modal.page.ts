import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';

@Component({
  selector: 'app-qr-modal',
  templateUrl: './qr-modal.page.html',
  styleUrls: ['./qr-modal.page.scss'],
})
export class QrModalPage implements OnInit {

  // Data passed in by componentProps
  @Input() qrCodeValue: string;
  title;
  elementType;
  value;
  errorCorrectionLevel;
  constructor(public modalController: ModalController) { }

  ngOnInit() {
      
    this.title = 'app';
    this.elementType = NgxQrcodeElementTypes.URL;
    this.value = this.qrCodeValue;
    this.errorCorrectionLevel = NgxQrcodeErrorCorrectionLevels.QUARTILE;
  }
  
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
