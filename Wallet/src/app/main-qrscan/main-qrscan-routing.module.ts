import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainQrscanPage } from './main-qrscan.page';

const routes: Routes = [
  {
    path: '',
    component: MainQrscanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainQrscanPageRoutingModule {}
