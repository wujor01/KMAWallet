import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MainQrscanPageRoutingModule } from './main-qrscan-routing.module';

import { MainQrscanPage } from './main-qrscan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainQrscanPageRoutingModule
  ],
  declarations: [MainQrscanPage]
})
export class MainQrscanPageModule {}
