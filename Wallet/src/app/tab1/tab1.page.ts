import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { QrModalPage } from './../qr-modal/qr-modal.page';
import { ApiService } from './../api.service';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  myInfo = localStorage.myInfo ? JSON.parse(localStorage.myInfo) : {};
  myWallets = localStorage.myWallets ? JSON.parse(localStorage.myWallets) : [];

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private plt: Platform,
    public modalController: ModalController,
    private api: ApiService
  ) {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        if(ev.urlAfterRedirects == '/tabs/tab1'){
          //this.myInfo = JSON.parse(localStorage.myInfo);
          //this.myWallets = JSON.parse(localStorage.myWallets);
          this.ngOnInit();
          console.log(ev);
        }
    }
  });
  }
  ngOnInit(): void {
    //this.loading.present();
    //console.log(localStorage.token);
    this.api.getMyInfo()
    .then(res =>{
      //this.loading.dismiss();
      this.myInfo = res;
      console.log(res);
    })
    .catch(err => {
      alert(err.error);
      //this.loading.dismiss();
    });

    this.api.getMyWallets()
    .then(res =>{
      this.myWallets = res;
      console.log(res);
    })
    .catch(err => alert(JSON.stringify(err)));
  }

  async presentModal(walletModel) {
    const modal = await this.modalController.create({
      component: QrModalPage,
      componentProps: {
        'qrCodeValue': walletModel.walletid
      }
    });
    return await modal.present();
  }
}
