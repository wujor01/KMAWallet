import { Component, OnInit } from '@angular/core';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  title = 'app';
  elementType = NgxQrcodeElementTypes.URL;
  value = JSON.parse(localStorage.myWallets);
  errorCorrectionLevel = NgxQrcodeErrorCorrectionLevels.QUARTILE;

  logout(){
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
