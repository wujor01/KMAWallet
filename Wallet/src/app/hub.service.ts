import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalNotifications } from '@capacitor/local-notifications';
import { Platform } from '@ionic/angular';
import * as signalR from '@microsoft/signalr';
import { platform } from 'os';
import { environment } from 'src/environments/environment';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class HubService implements OnInit{

  constructor(
    private router: Router, 
    private api: ApiService,
    public platform: Platform) {
   }

  async ngOnInit() {
    await LocalNotifications.requestPermissions();
  }

   private hubConnection: signalR.HubConnection;

   public startConnection = async()  => {
     this.hubConnection = new signalR.HubConnectionBuilder()
                          .withUrl(environment.urlEndpoint + "/chathub")
                          .configureLogging(signalR.LogLevel.Information)
                          .build();
                          
    await this.hubConnection
      .start()
      .then(() => {
        console.log("SignalR Connected.");
      })
      .catch(err => console.log('Error while starting connection: ' + err))
  }

  public addHubListener = () => {
    this.hubConnection.on("ReceiveMessage", (user, message) => {
      alert('Tin nhắn từ ' + user + ': ' + message);
    });

    this.hubConnection.on("NotifyMessage", (message) => {
      if(this.platform.is("mobile")){
        LocalNotifications.schedule({
          notifications: [
            {
              title: 'Thông báo',
              body: message,
              id: 2,
              extra: {
                data: message
              },
              iconColor: '#0000FF'
            }
          ]
        })
      }else
        alert('Thông báo: ' + message);
    });

    this.hubConnection.on("ErrorMessage", (message) => {
      if(message == 'error-token'){
        localStorage.clear();
        alert('Hết hạn phiên đăng nhập, vui lòng đăng nhập lại');
      }
    });

    this.hubConnection.on("LogMessage", (message) => {
      console.log(message);
    });
  }

  public sendMessage = (username, message) => {
    this.hubConnection.invoke("SendMessage", username, message);
  }
  
  public sendAuth = (token: string) => {
      this.hubConnection.invoke("SendAuth", token);
  }
}
