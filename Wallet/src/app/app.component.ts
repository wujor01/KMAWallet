import { Component, OnInit } from '@angular/core';
import { HubService } from './hub.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private hub: HubService) {}

  ngOnInit(): void {
    this.hub.startConnection().then(res => {
      //Đã từng đăng nhập trước đó
      if (localStorage.token) {
        this.hub.sendAuth(localStorage.token);
      }
    });
    this.hub.addHubListener();    
  }
}
