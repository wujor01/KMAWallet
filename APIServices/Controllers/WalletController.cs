﻿using APIServices.Common;
using APIServices.Models;
using APIServices.Services;
using APIServices.Services.HubSignalR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace APIServices.Controllers
{
    [ApiController]
    public class WalletController : BaseController
    {
        private IWalletService _walletService;
        private IUserService _userService;
        private IHubContext<ChatHub> _hubContext;
        private readonly AppSettings _appSettings;

        public WalletController(IWalletService walletService, IUserService userService, IHubContext<ChatHub> hubContext, IOptions<AppSettings> appSettings)
        {
            _walletService = walletService;
            _userService = userService;
            _hubContext = hubContext;
            _appSettings = appSettings.Value;
        }

        static Random random = new Random();

        [HttpPost]
        public IActionResult CreateWallet(string walletName)
        {
            try
            {
                UserModel currentUser = (UserModel)HttpContext.Items["User"];
                Crypto crypto = new Crypto();

                WalletModel newWallet = new WalletModel
                {
                    WALLETNAME = walletName,
                    USERID = currentUser.USERID
                };
                newWallet.PRIVATEKEY = DateTime.Now.Ticks.ToString();
                //Tạo khóa công khai, lấy địa chỉ ví
                byte[] publicKeyRaw = null;
                newWallet.PUBLICKEY = crypto.GetPublicKeyFromPrivateKeyEx(newWallet.PRIVATEKEY, out publicKeyRaw);
                HashAlgorithm sha = SHA512.Create();
                byte[] address = publicKeyRaw.TakeLast(20).ToArray();

                //Mã hóa privatekey
                newWallet.PRIVATEKEY = Convert.ToBase64String(crypto.Encrypt(Encoding.ASCII.GetBytes(newWallet.PRIVATEKEY), Encoding.ASCII.GetBytes(_appSettings.Secret)));

                newWallet.WALLETID = Base58Encoding.Encode(sha.ComputeHash(address));

                _walletService.CreateWallet(newWallet);

                return Ok(newWallet);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetMyWallets()
        {
            try
            {
                UserModel currentUser = (UserModel)HttpContext.Items["User"];
                return Ok(_walletService.GetWalletByUserId(currentUser.USERID));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult NewTransaction(string walletid, string toWalletid, double amount, string content)
        {
            try
            {
                WalletModel wallet = _walletService.GetWalletById(walletid);
                WalletModel toWallet = _walletService.GetWalletById(toWalletid.Trim());
                if (toWallet == null)
                {
                    return BadRequest("Không có thông tin người nhận, vui lòng kiểm tra lại");
                }

                Crypto cryto = new Crypto();

                var transaction = new Transaction();
                transaction.FromPublicKey = wallet.PUBLICKEY;
                transaction.ToPublicKey = toWallet.PUBLICKEY;
                transaction.Amount = amount;
                //Giải mã khóa bí mật
                wallet.PRIVATEKEY = Encoding.ASCII.GetString(cryto.Decrypt(Convert.FromBase64String(wallet.PRIVATEKEY), Encoding.ASCII.GetBytes(_appSettings.Secret)));

                transaction.Signature = cryto.Signature(wallet.PRIVATEKEY, transaction.ToString());

                TransactionModel transactionModel = new TransactionModel();
                transactionModel.WALLETID = wallet.WALLETID;
                transactionModel.TOWALLETID = toWallet.WALLETID;
                transactionModel.AMOUNT = amount;
                transactionModel.SIGNATURE = transaction.Signature;
                transactionModel.CONTENT = content;

                IData objIData = Data.CreateData(_appSettings.ConnectionString);
                try
                {
                    objIData.BeginTransaction();
                    Guid transactionid = _walletService.NewTransaction(transactionModel, objIData);
                    objIData.CommitTransaction();
                    //var objTransaction = _walletService.GetTransactionById(transactionid, objIData);
                    //bool isValidTransaction = cryto.VerifySignature(transaction.ToString(), transaction.FromPublicKey, transaction.Signature);
                    //if (isValidTransaction)
                    //{
                    //    objIData.CommitTransaction();
                    //
                    //    var recivedUser = _userService.GetById(toWallet.USERID);
                    //    _hubContext.Clients.Client(recivedUser.CONNECTIONID).SendAsync("NotifyMessage", $"Biên động số dư: +{transactionModel.AMOUNT}. Chi tiết xem tại Lịch sử giao dịch");
                    //    return Ok(new { objTransaction, isValidTransaction = isValidTransaction });
                    //}
                    //else
                    //    return BadRequest(new { objTransaction, isValidTransaction = isValidTransaction });

                    return Ok(new { transaction = transaction, transactionId = transactionid , toWallet = toWallet });
                }
                catch (Exception)
                {
                    objIData.RollBackTransaction();
                    throw;
                }
                finally
                {
                    objIData.Disconnect();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult VerifyTransaction([FromBody] VerifyTransactionFromBody body)
        {
            try
            {
                Crypto cryto = new Crypto();
                Transaction transaction = body.transaction;
                WalletModel toWallet = body.toWallet;
                Guid transactionId = body.transactionId;

                UserModel currentUser = (UserModel)HttpContext.Items["User"];
                WalletModel fromWallet = _walletService.GetWalletByUserId(currentUser.USERID).FirstOrDefault();

                bool isValidTransaction = cryto.VerifySignature(transaction.ToString(), fromWallet.PUBLICKEY, transaction.Signature);
                if (isValidTransaction)
                {
                    _walletService.UpdateProcessType(transactionId, 2);
                    var recivedUser = _userService.GetById(toWallet.USERID);
                    _hubContext.Clients.Client(recivedUser.CONNECTIONID).SendAsync("NotifyMessage", $"Biên động số dư: +{transaction.Amount}. Chi tiết xem tại Lịch sử giao dịch");
                    return Ok("Chuyển tiền thành công!");
                }
                else
                {
                    _walletService.UpdateProcessType(transactionId, 3);
                    return BadRequest("Chuyển tiền thất bại, xác thực không thành công");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public class VerifyTransactionFromBody
        {
            public Transaction transaction { get; set; }
            public WalletModel toWallet { get; set; }
            public Guid transactionId { get; set; }
        }

        [HttpGet]
        public IActionResult GetAllMyTransaction()
        {
            try
            {
                UserModel currentUser = (UserModel)HttpContext.Items["User"];
                return Ok(_walletService.GetAllTransactionByUserId(currentUser.USERID));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
