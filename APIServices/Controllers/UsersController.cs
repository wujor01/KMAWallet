﻿using APIServices.Models;
using APIServices.Security;
using APIServices.Services;
using APIServices.Services.HubSignalR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace APIServices.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IHubContext<ChatHub> _hubContext;

        public UsersController(IUserService userService, IHubContext<ChatHub> hubContext)
        {
            _userService = userService;
            _hubContext = hubContext;
        }
        [HttpPost]
        public IActionResult Login(AuthenticateRequest model)
        {
            try
            {
                //Hash password
                HashAlgorithm sha = SHA512.Create();
                model.Password = Convert.ToBase64String(sha.ComputeHash(Encoding.ASCII.GetBytes(model.Password)));

                var response = _userService.Authenticate(model);

                if (response == null)
                    return BadRequest("Username or password is incorrect");

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost]
        public IActionResult Register(UserModel model)
        {
            try
            {
                //Hash password
                HashAlgorithm sha = SHA512.Create();
                model.PASSWORD = Convert.ToBase64String(sha.ComputeHash(Encoding.ASCII.GetBytes(model.PASSWORD)));

                _userService.Register(model);
                return Ok("Tạo tài khoản thành công.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [JWTAuthorize]
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var objUserLogin = (UserModel)Request.HttpContext.Items["User"];

                var lstUser = _userService.GetAll();
                return Ok(lstUser);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [JWTAuthorize]
        [HttpGet]
        public IActionResult GetMyInfo()
        {
            try
            {
                var objUserLogin = (UserModel)Request.HttpContext.Items["User"];
                return Ok(objUserLogin);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpGet]
        public IActionResult GetUserByWalletId(string walletId)
        {
            try
            {
                var result = _userService.GetUserByWalletId(walletId);
                if (result == null)
                {
                    return BadRequest("Không tìm thấy thông tin người nhận, vui lòng kiểm tra lại!");
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
    }
}
