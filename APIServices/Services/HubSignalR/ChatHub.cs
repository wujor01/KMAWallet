﻿using APIServices.Models;
using APIServices.Security;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIServices.Services.HubSignalR
{
    public class ChatHub : Hub
    {
        private readonly IOptions<AppSettings> _appSettings;
        private readonly IUserService _userService;
        private UserModel currentUser;

        public ChatHub(IOptions<AppSettings> appSettings, IUserService userService)
        {
            _appSettings = appSettings;
            _userService = userService;
        }

        public async Task SendAuth(string token)
        {
            try
            {
                if (token != null)
                {
                    attachUserToHubContext(Context, _userService, token);
                    currentUser = CurrentUser();
                    if (currentUser == null)
                    {
                        await Clients.Client(Context.ConnectionId).SendAsync("LogMessage", "Kết nối đến SignalRHub-Server thất bại!");
                    }
                    else
                    {
                        _userService.UpdateConnectionId(currentUser.USERNAME, Context.ConnectionId);
                        await Clients.Client(Context.ConnectionId).SendAsync("LogMessage", "Kết nối đến SignalRHub-Server thành công!");
                    }
                }
            }
            catch (Exception)
            {
                await Clients.Client(Context.ConnectionId).SendAsync("ErrorMessage", "error-token");
            }
        }

        public async Task SendMessage(string username, string message)
        {
            try
            {
                currentUser = CurrentUser();
                //Gửi all
                if (username == null)
                {
                    await Clients.All.SendAsync("NotifyMessage", message);
                }
                else//Gửi đến đích danh connectionId
                {
                    var user = _userService.GetByUsername(username);
                    if (user == null)
                    {
                        await Clients.Client(Context.ConnectionId).SendAsync("NotifyMessage", "Không có user này!");
                    }
                    else if (user.CONNECTIONID == null)
                    {
                        await Clients.Client(Context.ConnectionId).SendAsync("NotifyMessage", "User này không online!");
                    }
                    else
                    {
                        await Clients.Client(Context.ConnectionId).SendAsync("NotifyMessage", "Gửi tin nhắn thành công!");
                        await Clients.Client(user.CONNECTIONID).SendAsync("NotifyMessage", currentUser.USERNAME + ": " + message);
                    }
                }
            }
            catch (Exception ex)
            {
                await Clients.Client(Context.ConnectionId).SendAsync("NotifyMessage", ex.Message);
            }
        }

        private void attachUserToHubContext(HubCallerContext context, IUserService userService, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Value.Secret);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var userId = long.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);

                // attach user to context on successful jwt validation
                context.Items["User"] = userService.GetById(userId);
            }
            catch (Exception)
            {
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
                throw;
            }
        }

        private UserModel CurrentUser()
        {
            return (UserModel)Context?.Items["User"];
        }
    }
}