﻿using APIServices.Common;
using APIServices.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace APIServices.Services
{
    public interface IUserService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        List<UserModel> GetAll();
        UserModel GetById(long id);
        void UpdateConnectionId(string username, string connectionId);
        UserModel GetByUsername(string username);
        void Register(UserModel model);
        UserModel GetUserByWalletId(string walletId);
    }

    public class UserService : DAOHelper, IUserService
    {
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        /// <summary>
        /// Hàm xác thực tài khoản, tạo token
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = ExecStoreToObject<UserModel>(new List<object> { model.Username, model.Password},"masterdata.user_getbyuserpwd", _appSettings.ConnectionString).FirstOrDefault();

            if (user == null) return null;

            var token = generateJwtToken(user);

            return new AuthenticateResponse(user, token);
        }

        public List<UserModel> GetAll()
        {
            string query = @"
                select *
                from masterdata.user
            ";

            return DAOHelper.ExecQueryToObject<UserModel>(query, _appSettings.ConnectionString);
        }
        /// <summary>
        /// Lấy thông tin User
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserModel GetById(long id)
        {
            return ExecStoreToObject<UserModel>(new List<object> { id }, "masterdata.user_getbyid", _appSettings.ConnectionString).FirstOrDefault();
        }

        /// <summary>
        /// Tạo jwt token
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private string generateJwtToken(UserModel user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.USERID.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public void UpdateConnectionId(string username, string connectionId)
        {
            ExecStoreNoneQuery(new List<object> { username, connectionId}, "masterdata.user_connectionid_upd", _appSettings.ConnectionString);
        }

        public UserModel GetByUsername(string username)
        {
            return ExecStoreToObject<UserModel>(new List<object> { username }, "masterdata.user_getbyusername", _appSettings.ConnectionString).FirstOrDefault();
        }

        public void Register(UserModel model)
        {
            ExecStoreNoneQuery(new List<object> { model.USERNAME, model.PASSWORD, model.EMAIL, model.IDCARD, model.FULLNAME, model.GENDER, model.BIRTHDAY, model.PHONENUMBER }, "masterdata.user_register", _appSettings.ConnectionString);
        }

        public UserModel GetUserByWalletId(string publicKey)
        {
            return ExecStoreToObject<UserModel>(new List<object> { publicKey }, "masterdata.user_getbywalletid", _appSettings.ConnectionString).FirstOrDefault();
        }
    }
}
