﻿using APIServices.Common;
using APIServices.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIServices.Services
{
    public interface IWalletService
    {
        void CreateWallet(WalletModel model);
        WalletModel GetWalletById(string walletid);
        List<WalletModel> GetWalletByUserId(long userid);
        Guid NewTransaction(TransactionModel model, IData objData = null);
        TransactionModel GetTransactionById(Guid transactionid, IData objData = null);
        List<TransactionModel> GetAllTransactionByUserId(long uSERID);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="processType">1: Đang xử lý, 2: Đã xác nhận, 3: Từ chối</param>
        void UpdateProcessType(Guid transactionId, short processType);
    }

    public class WalletService : DAOHelper, IWalletService
    {
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        private readonly AppSettings _appSettings;

        public WalletService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public void CreateWallet(WalletModel model)
        {
            ExecStoreNoneQuery(new List<object> { model.WALLETID, model.USERID, model.WALLETNAME, model.PRIVATEKEY, model.PUBLICKEY }, "masterdata.wallet_add", _appSettings.ConnectionString);
        }

        public WalletModel GetWalletById(string walletid)
        {
            return ExecStoreToObject<WalletModel>(new List<object> { walletid }, "masterdata.wallet_getbyid", _appSettings.ConnectionString).FirstOrDefault();
        }

        public List<WalletModel> GetWalletByUserId(long userid)
        {
            return ExecStoreToObject<WalletModel>(new List<object> { userid }, "masterdata.wallet_getbyuserid", _appSettings.ConnectionString);
        }

        public Guid NewTransaction(TransactionModel model, IData objData = null)
        {
            if (objData != null)
                objDataAccess = objData;

            return Guid.Parse(ExecStoreToString(new List<object> { model.WALLETID, model.TOWALLETID, model.AMOUNT, model.CONTENT, model.SIGNATURE}, "masterdata.transaction_add", _appSettings.ConnectionString));
        }

        public TransactionModel GetTransactionById(Guid transactionid, IData objData = null)
        {
            if (objData != null)
                objDataAccess = objData;

            return ExecStoreToObject<TransactionModel>(new List<object> { transactionid }, "masterdata.transaction_getbyid", _appSettings.ConnectionString).FirstOrDefault();
        }

        public List<TransactionModel> GetAllTransactionByUserId(long userid)
        {
            return ExecStoreToObject<TransactionModel>(new List<object> { userid }, "masterdata.transaction_getbyuserid", _appSettings.ConnectionString);
        }

        public void UpdateProcessType(Guid transactionId, Int16 processType)
        {
            ExecStoreNoneQuery(new List<object> { transactionId, processType }, "masterdata.transaction_processtypeupd", _appSettings.ConnectionString);
        }
    }
}
