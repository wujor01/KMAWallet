﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace APIServices.Common
{
    public class Transaction
    {
        public string FromPublicKey { get; set; }
        public string ToPublicKey { get; set; }
        public double Amount { get; set; }
        public string Signature { get; set; }

        public override string ToString()
        {
            return $"{this.Amount}:{this.FromPublicKey}:{this.ToPublicKey}";
        }
    }
}
