﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace APIServices.Models
{
	public class WalletModel
	{
		public string WALLETID { get; set; }
		public Int64 USERID { get; set; }
		public string WALLETNAME { get; set; }
		public double BALANCE { get; set; }
		public string PRIVATEKEY { get; set; }
		public string PUBLICKEY { get; set; }
		public DateTime? CREATEDDATE { get; set; }
        public string FULLNAME { get; set; }
    }
}
