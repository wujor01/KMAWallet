﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIServices.Models
{
    public class AuthenticateResponse
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }


        public AuthenticateResponse(UserModel user, string token)
        {
            Id = user.USERID;
            FullName = user.FULLNAME;
            Username = user.USERNAME;
            Token = token;
        }
    }
}
