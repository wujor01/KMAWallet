﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIServices.Models
{
    public class TransactionModel
	{
		public Guid TRANSACTIONID { get; set; }
		public string WALLETID { get; set; }
		public Int16 PROCESSTYPE { get; set; }
		public double AMOUNT { get; set; }
		public string CONTENT { get; set; }
		public string TOWALLETID { get; set; }
		public string SIGNATURE { get; set; }
		public DateTime CREATEDDATE { get; set; }
        public string TOPUBLICKEYWALLET { get; set; }
		/// <summary>
		/// Tên ví người gửi
		/// </summary>
        public string WALLETNAME { get; set; }
        public string RECEIVEDUSERNAME { get; set; }
        public string RECEIVEDFULLNAME { get; set; }
		public string CREATEDUSERNAME { get; set; }
		public string CREATEDFULLNAME { get; set; }

	}
}
