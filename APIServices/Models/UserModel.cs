﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace APIServices.Models
{
	public class UserModel
	{
		[JsonIgnore]
		public Int64 USERID { get; set; }
		public string FULLNAME { get; set; }
		public string USERNAME { get; set; }
		public string PASSWORD { get; set; }
		public string EMAIL { get; set; }
		public string PHONENUMBER { get; set; }
		[JsonIgnore]
		public string CONNECTIONID { get; set; }
		[JsonIgnore]
		public DateTime? LASTSIGNIN { get; set; }
		public string IDCARD { get; set; }
		/// <summary>
		/// 0.Nữ, 1. Nam
		/// </summary>
		public Int32 GENDER { get; set; }
		public DateTime? BIRTHDAY { get; set; }
		[JsonIgnore]
		public string IMAGEBASE64 { get; set; }
		[JsonIgnore]
		public DateTime? CREATEDDATE { get; set; }
		[JsonIgnore]
		public DateTime? UPDATEDDATE { get; set; }
		/// <summary>
		/// wallet.publickey 
		/// </summary>
        public string WALLETID { get; set; }
    }
}
