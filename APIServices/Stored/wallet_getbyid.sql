CREATE OR REPLACE FUNCTION "masterdata"."wallet_getbyid"("p_walletid" varchar)
  RETURNS "pg_catalog"."refcursor" AS $BODY$
DECLARE ref refcursor;
BEGIN
	OPEN ref FOR 
SELECT wallet.*, wallet_public.publickey
FROM masterdata.wallet
LEFT JOIN masterdata.wallet_public ON wallet_public.walletid = wallet.walletid
WHERE wallet.walletid = p_walletid
AND isdeleted = false;
  RETURN ref;                                                  
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100