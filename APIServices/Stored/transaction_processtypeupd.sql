CREATE OR REPLACE FUNCTION "masterdata"."transaction_processtypeupd"("p_transactionid" uuid, "p_processtype" int2)
  RETURNS "pg_catalog"."void" AS $BODY$
	DECLARE 
	v_walletid VARCHAR;
	v_towalletid VARCHAR;
	v_amount float8;
BEGIN
	
	IF p_processtype = 2 THEN
	SELECT "transaction".walletid, "transaction".towalletid, "transaction".amount INTO v_walletid, v_towalletid, v_amount
	FROM masterdata."transaction"
	WHERE transactionid = p_transactionid;
	
	UPDATE masterdata.wallet 
	SET balance= balance - v_amount
	WHERE walletid = v_walletid;
	
	UPDATE masterdata.wallet 
	SET balance= balance + v_amount
	WHERE walletid = v_towalletid;	
	END IF;	
	
	UPDATE "masterdata"."transaction" SET "processtype" = p_processtype WHERE "transactionid" = p_transactionid;
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100