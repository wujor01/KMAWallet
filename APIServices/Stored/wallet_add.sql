CREATE OR REPLACE FUNCTION "masterdata"."wallet_add"("p_walletid" varchar, "p_userid" int8, "p_walletname" varchar, "p_privatekey" varchar, "p_publickey" varchar)
  RETURNS "pg_catalog"."void" AS $BODY$
BEGIN
INSERT INTO masterdata.wallet
					(
					walletid,
					userid , 
					walletname,
					privatekey,
					publickey,
					createddate					
					)
				VALUES
				(
					p_walletid,
					p_userid , 
					p_walletname,
					p_privatekey,
					p_publickey,
					now()					
				);
				
INSERT INTO masterdata.wallet_public
	(
	walletid,
	publickey					
	)
VALUES
(
	p_walletid,
	p_publickey		
);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100