CREATE OR REPLACE FUNCTION "masterdata"."transaction_add"("p_walletid" varchar, "p_towalletid" varchar, "p_amount" float8, "p_content" varchar, "p_signature" varchar)
  RETURNS "pg_catalog"."uuid" AS $BODY$
	DECLARE 
	v_towalletid VARCHAR;
	t_transactionid uuid;
	v_balancenow float8;
BEGIN

SELECT walletid INTO v_towalletid
FROM masterdata.wallet
WHERE walletid = p_towalletid
AND isdeleted = FALSE;

	if p_walletid = p_towalletid
 THEN
   raise EXCEPTION '[+Ví gửi không thể là ví nhận !+]';
 end if;

 if v_towalletid is null
 THEN
   raise EXCEPTION '[+Không tìm thấy thông tin người nhận % vùi lòng kiểm tra lại !+]',TRIM(p_towalletid);
 end if;
 
 SELECT balance INTO v_balancenow
 FROM masterdata.wallet
 WHERE walletid = p_walletid;

  if (v_balancenow - p_amount) < 0
 THEN
   raise EXCEPTION '[+Số dư của bạn không đủ để thực hiện giao dịch này !+]';
 end if;

INSERT INTO masterdata.transaction
					(
					walletid,
					towalletid,
					amount,
					content,
					signature,
					processtype,
					createddate
					)
				VALUES
				(
					p_walletid,
					v_towalletid,
					p_amount,
					p_content,
					p_signature,
					1,
					now()					
				) returning transactionid into t_transactionid;
				
				RETURN t_transactionid;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100