CREATE OR REPLACE FUNCTION "masterdata"."transaction_getbyid"("p_transactionid" uuid)
  RETURNS "pg_catalog"."refcursor" AS $BODY$
DECLARE ref refcursor;
BEGIN
	OPEN ref FOR 
SELECT * FROM masterdata.transaction
WHERE transactionid = p_transactionid;
  RETURN ref;                                                  
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100