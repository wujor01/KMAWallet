CREATE OR REPLACE FUNCTION "masterdata"."user_getbywalletid"("p_walletid" varchar)
  RETURNS "pg_catalog"."refcursor" AS $BODY$
DECLARE ref refcursor;
BEGIN
	OPEN ref FOR 
SELECT 
"wallet".userid,
"user".username,
"user".phonenumber,
"wallet".walletid
FROM masterdata."wallet"
LEFT JOIN masterdata."user"
ON "user".userid = wallet.userid
WHERE wallet.walletid = p_walletid
AND wallet.isdeleted = FALSE;
  RETURN ref;                                                  
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100