CREATE OR REPLACE FUNCTION masterdata.user_register(p_username VARCHAR, p_password VARCHAR, p_email VARCHAR, p_idcard VARCHAR, p_fullname VARCHAR, p_gender int4, p_birthday TIMESTAMP, p_phonenumber VARCHAR)
  RETURNS pg_catalog.void AS $BODY$
	DECLARE v_username VARCHAR; 
BEGIN
	SELECT username INTO v_username
	FROM masterdata.user
	WHERE username = p_username
	OR email = p_email
	OR phonenumber = p_phonenumber;

 if v_username is not null
 THEN
   raise EXCEPTION '[+Thông tin tài khoản, email hoặc số điện thoại đã được sử dụng, vui lòng kiểm tra lại !+]';
 end if;

INSERT INTO masterdata.user
					(
					username , 
					password,
					email,
					idcard,
					fullname,
					gender,
					birthday,
					phonenumber,
					createddate				
					)
				VALUES
				(
					p_username , 
					p_password,
					p_email,
					p_idcard,
					p_fullname,
					p_gender,
					p_birthday,
					p_phonenumber,
					now()					
				);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100