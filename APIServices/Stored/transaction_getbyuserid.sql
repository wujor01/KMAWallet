CREATE OR REPLACE FUNCTION "masterdata"."transaction_getbyuserid"("p_userid" int8)
  RETURNS "pg_catalog"."refcursor" AS $BODY$
DECLARE ref refcursor;
BEGIN
	OPEN ref FOR 
SELECT 
transaction.transactionid,
CASE
     WHEN "transaction".walletid = wallet.walletid THEN -amount
     WHEN "transaction".towalletid = wallet.walletid THEN amount
 END amount,
"transaction"."content",
"transaction".createddate,
"transaction".processtype,
"transaction".signature,
"transaction".towalletid,
"transaction".walletid,
wallet.walletname,
reuser.username receivedusername,
reuser.fullname receivedfullname,
creuser.username createduser,
creuser.fullname createdfullname
FROM masterdata.wallet
JOIN masterdata."transaction"
ON "transaction".walletid = wallet.walletid OR "transaction".towalletid = wallet.walletid
JOIN masterdata.wallet crewallet
ON crewallet.walletid = "transaction".walletid
LEFT JOIN masterdata."user" creuser
ON creuser.userid = crewallet.userid
JOIN masterdata.wallet rewallet
ON rewallet.walletid = "transaction".towalletid
LEFT JOIN masterdata."user" reuser
ON reuser.userid = rewallet.userid
WHERE wallet.userid = p_userid
ORDER BY "transaction".createddate DESC;
  RETURN ref;                                                  
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100