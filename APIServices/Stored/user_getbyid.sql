CREATE OR REPLACE FUNCTION "masterdata"."user_getbyid"("p_userid" int8)
  RETURNS "pg_catalog"."refcursor" AS $BODY$
DECLARE ref refcursor;
BEGIN
	OPEN ref FOR 
	SELECT * FROM masterdata."user"
	WHERE "user".userid = p_userid;
  RETURN ref;                                                  
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100