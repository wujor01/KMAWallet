CREATE OR REPLACE FUNCTION "masterdata"."user_getbypublickey"("p_publickey" VARCHAR)
  RETURNS "pg_catalog"."refcursor" AS $BODY$
DECLARE ref refcursor;
BEGIN
	OPEN ref FOR 
SELECT 
"wallet".userid,
"user".username,
"user".phonenumber,
"wallet".publickey
FROM masterdata."wallet"
LEFT JOIN masterdata."user"
ON "user".userid = wallet.userid
WHERE wallet.publickey = p_publickey
AND wallet.isdeleted = FALSE;
  RETURN ref;                                                  
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100